package ordination;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.Test;

public class OrdinationTest {

	Patient patient = null;
	Laegemiddel DSG = null;
	LocalDate startDate = LocalDate.of(2016, 02, 24);
	LocalDate slutDate = LocalDate.of(2016, 03, 24);
	PN O = new PN(startDate, slutDate, patient, DSG, 5);

	@Test
	public void testAntalDage1() {
		// Act
		int actualResult = O.antalDage();
		// Assert
		assertEquals(30, actualResult);
	}

	@Test
	public void testGivDosis1() {
		// Act
		boolean actualResult = O.givDosis(LocalDate.of(2016, 02, 29));
		// Assert
		assertEquals(true, actualResult);
	}

	@Test
	public void testGivDosis2() {
		// Tester givDosis på startDato
		// Act
		boolean actualResult = O.givDosis(LocalDate.of(2016, 02, 24));
		// Assert
		assertEquals(true, actualResult);
	}

	@Test
	public void testGivDosis3() {
		// Tester givDosis på slutDato
		// Act
		boolean actualResult = O.givDosis(LocalDate.of(2016, 03, 24));
		// Assert
		assertEquals(true, actualResult);
	}

	@Test
	public void testgetAntalGangeGivet() {
		// Act
		O.givDosis(LocalDate.of(2016, 03, 24));
		O.givDosis(LocalDate.of(2016, 03, 24));
		O.givDosis(LocalDate.of(2016, 03, 24));
		O.givDosis(LocalDate.of(2016, 03, 24));
		O.givDosis(LocalDate.of(2016, 03, 24));
		// Assert
		assertEquals(5, O.getAntalGangeGivet());
	}

	@Test
	public void samletDosis() {
		// Act
		O.givDosis(LocalDate.of(2016, 03, 24));
		O.givDosis(LocalDate.of(2016, 03, 24));
		O.givDosis(LocalDate.of(2016, 03, 24));
		O.givDosis(LocalDate.of(2016, 03, 24));
		O.givDosis(LocalDate.of(2016, 03, 24));
		// Assert
		assertEquals(25, O.samletDosis(), 0);
	}

	@Test
	public void doegnDosis() {
		// Act
		O.givDosis(LocalDate.of(2016, 02, 26));
		O.givDosis(LocalDate.of(2016, 02, 27));
		O.givDosis(LocalDate.of(2016, 02, 28));
		// Assert
		assertEquals(5, O.doegnDosis(), 0);
	}
}
