package ordination;

import java.time.LocalDate;

public class DagligFast extends Ordination {

	private Dosis[] doser;

	public DagligFast(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel) {
		super(startDen, slutDen, patient, laegemiddel);
		doser = new Dosis[4];

	}

	@Override
	public double samletDosis() {
		double samletDosis;
		samletDosis = this.doegnDosis() * super.antalDage();
		return samletDosis;
	}

	@Override
	public double doegnDosis() {
		double doegnDosis;
		doegnDosis = doser[0].getAntal() + doser[1].getAntal() + doser[2].getAntal() + doser[3].getAntal();
		return doegnDosis;
	}

	@Override
	public String getType() {
		return "DagligFast";
	}

	public Dosis[] getDoser() {
		return doser;
	}

	public void setDoser(Dosis[] doser) {
		this.doser = doser;
	}

}
