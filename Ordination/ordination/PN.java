package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;

public class PN extends Ordination {

	private double antalEnheder;
	private int antalGangeGivet = 0;
	private ArrayList<LocalDate> datoGivet = new ArrayList<>();

	public PN(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel, double antalEnheder) {
		super(startDen, slutDen, patient, laegemiddel);
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen) {
		if ((givesDen.isAfter(getStartDen().minusDays(1)) && givesDen.isBefore(getSlutDen().plusDays(1)))) {
			datoGivet.add(givesDen);
			antalGangeGivet++;
			return true;
		}
		return false;
	}

	@Override
	public double doegnDosis() {
		Collections.sort(datoGivet);
		int antalDageGivet = 1;
		if (datoGivet.size() > 0) {
			antalDageGivet = (int) ChronoUnit.DAYS.between(datoGivet.get(0), datoGivet.get(datoGivet.size() - 1)) + 1;
		}
		double doegnDosis = (antalGangeGivet * antalEnheder) / antalDageGivet;

		return doegnDosis;
	}

	@Override
	public double samletDosis() {
		return antalGangeGivet * antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * @return
	 */
	public int getAntalGangeGivet() {
		return antalGangeGivet;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	@Override
	public String getType() {
		return "PN";
	}

	public ArrayList<LocalDate> getDatoGivet() {
		return new ArrayList<LocalDate>(datoGivet);
	}

	public void setDatoGivet(ArrayList<LocalDate> datoGivet) {
		this.datoGivet = datoGivet;
	}

	public void addDatoGivet(LocalDate dato) {
		datoGivet.add(dato);
	}

}
