package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {

	private ArrayList<Dosis> doser = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel) {
		super(startDen, slutDen, patient, laegemiddel);
	}

	public void opretDosis(LocalTime tid, double antal) {
		Dosis dagligSkæv = new Dosis(tid, antal);
		doser.add(dagligSkæv);

	}

	@Override
	public double samletDosis() {
		double samletDosis = 0;
		samletDosis = this.doegnDosis() * super.antalDage();
		return samletDosis;

	}

	@Override
	public double doegnDosis() {
		double doegnDosis = 0;
		for (Dosis d : doser) {
			doegnDosis += d.getAntal();
		}
		return doegnDosis;

	}

	@Override
	public String getType() {
		return "DagligSkæv";

	}

	public ArrayList<Dosis> getDoser() {
		return new ArrayList<Dosis>(doser);

	}

	public void setDoser(ArrayList<Dosis> doser) {
		this.doser = doser;
	}

	public void addDoser(Dosis dose) {
		doser.add(dose);
	}
}