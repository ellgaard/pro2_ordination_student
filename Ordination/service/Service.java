package service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Dosis;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import storage.Storage;

public class Service {
	private Storage storage;
	private static Service service;

	private Service() {
		storage = new Storage();
	}

	public static Service getService() {
		if (service == null) {
			service = new Service();
		}
		return service;
	}

	public static Service getTestService() {
		return new Service();
	}

	public Patient createPatient(String cprnr, String navn, double vaegt) {
		Patient patient = new Patient(cprnr, navn, vaegt);
		storage.addPatient(patient);
		return patient;
	}

	/**
	 * Hvis startDato er efter slutDato kastes en IllegalArgumentException og
	 * ordinationen oprettes ikke
	 *
	 * @return opretter og returnerer en PN ordination.
	 */
	public PN opretPNOrdination(LocalDate startDen, LocalDate slutDen,
			Patient patient, Laegemiddel laegemiddel, double antal) {
		if (slutDen.isBefore(startDen)) {
			throw new IllegalArgumentException("Slutdato er før startdato");
		}
		if (antal < 1) {
			throw new IllegalArgumentException(
					"Antal enheder skal mindst være 1");
		}
		PN EB = new PN(startDen, slutDen, patient, laegemiddel, antal);
		patient.addOrdinationer(EB);
		laegemiddel.addOrdination(EB);

		return EB;
	}

	/**
	 * Opretter og returnerer en DagligFast ordination. Hvis startDato er efter
	 * slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke
	 */
	public DagligFast opretDagligFastOrdination(LocalDate startDen,
			LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
			double morgenAntal, double middagAntal, double aftenAntal,
			double natAntal) {
		if (slutDen.isBefore(startDen)) {
			throw new IllegalArgumentException("Slutdato er før startdato");
		}
		if (morgenAntal < 0 || middagAntal < 0 || aftenAntal < 0
				|| natAntal < 0) {
			throw new IllegalArgumentException(
					"Antal enheder må ikke være negativt");
		}
		DagligFast DF = new DagligFast(startDen, slutDen, patient, laegemiddel);
		Dosis d1 = new Dosis(LocalTime.of(8, 00), morgenAntal);
		Dosis d2 = new Dosis(LocalTime.of(12, 00), middagAntal);
		Dosis d3 = new Dosis(LocalTime.of(18, 00), aftenAntal);
		Dosis d4 = new Dosis(LocalTime.of(23, 00), natAntal);
		Dosis[] doser = new Dosis[4];
		doser[0] = d1;
		doser[1] = d2;
		doser[2] = d3;
		doser[3] = d4;
		DF.setDoser(doser);
		patient.addOrdinationer(DF);
		laegemiddel.addOrdination(DF);
		return DF;
	}

	/**
	 * Opretter og returnerer en DagligSkæv ordination. Hvis startDato er efter
	 * slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke
	 */
	public DagligSkaev opretDagligSkaevOrdination(LocalDate startDen,
			LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
			LocalTime[] klokkeSlet, double[] antalEnheder) {
		if (slutDen.isBefore(startDen)) {
			throw new IllegalArgumentException("Slutdato er før startdato");
		}
		for (double D : antalEnheder) {
			if (D <= 0) {
				throw new IllegalArgumentException(
						"Antal enheder skal være større end 0");
			}
		}
		DagligSkaev DS = new DagligSkaev(startDen, slutDen, patient,
				laegemiddel);
		for (int i = 0; i < klokkeSlet.length; i++) {
			DS.opretDosis(klokkeSlet[i], antalEnheder[i]);
		}
		patient.addOrdinationer(DS);
		laegemiddel.addOrdination(DS);
		return DS;
	}

	/**
	 * En dato for hvornår ordinationen anvendes tilføjes ordinationen. Hvis
	 * datoen ikke er indenfor ordinationens gyldighedsperiode kastes en
	 * IllegalArgumentException
	 */
	public void ordinationPNAnvendt(PN ordination, LocalDate dato) {
		if (ordination.getSlutDen().isBefore(ordination.getStartDen())) {
			throw new IllegalArgumentException(
					"Dato for ordination er ikke i gyldighedsperioden");
		}
		ordination.givDosis(dato);
	}

	/**
	 * Den anbefalede dosis for den pågældende patient (der skal tages hensyn
	 * til patientens vægt). Det er en forskellig enheds faktor der skal
	 * anvendes, og den er afhængig af patientens vægt.
	 */
	public double anbefaletDosisPrDoegn(Patient patient, Laegemiddel laegemiddel) {
		double result;
		if (patient.getVaegt() < 25) {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnLet();
		} else if (patient.getVaegt() > 120) {
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnTung();
		} else {
			result = patient.getVaegt()
					* laegemiddel.getEnhedPrKgPrDoegnNormal();
		}
		return result;
	}

	/**
	 * For et givent vægtinterval og et givent lægemiddel, hentes antallet af
	 * ordinationer.
	 */
	public int antalOrdinationerPrVægtPrLægemiddel(double vægtStart,
			double vægtSlut, Laegemiddel laegemiddel) {
		int AntalOrdi = 0;
		if (vægtStart < 0 || vægtSlut < vægtStart) {
			throw new IllegalArgumentException("Ugyldig vægt indtastet");
		}
		for (Patient pat : getAllPatienter()) {
			if (pat.getVaegt() >= vægtStart && pat.getVaegt() <= vægtSlut) {
				for (int i = 0; i < pat.getOrdinationer().size(); i++) {
					if (pat.getOrdinationer().get(i).getLaegemiddel() == laegemiddel) {
						AntalOrdi++;
					}
				}
			}
		}
		return AntalOrdi;
	}

	public List<Patient> getAllPatienter() {
		return storage.getAllPatienter();
	}

	public List<Laegemiddel> getAllLaegemidler() {
		return storage.getAllLaegemidler();
	}

	/**
	 * Metode der kan bruges til at checke at en startDato ligger før en
	 * slutDato.
	 *
	 * @return true hvis startDato er før slutDato, false ellers.
	 */
	private boolean checkStartFoerSlut(LocalDate startDato, LocalDate slutDato) {
		boolean result = true;
		if (slutDato.compareTo(startDato) < 0) {
			result = false;
		}
		return result;
	}

	public void createSomeObjects() {
		storage.addPatient(new Patient("Jane Jensen", "121256-0512", 63.4));
		storage.addPatient(new Patient("Finn Madsen", "070985-1153", 83.2));
		storage.addPatient(new Patient("Hans Jørgensen", "050972-1233", 89.4));
		storage.addPatient(new Patient("Ulla Nielsen", "011064-1522", 59.9));
		storage.addPatient(new Patient("Ib Hansen", "090149-2529", 87.7));

		storage.addLaegemiddel(new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15,
				0.16, "Styk"));
		storage.addLaegemiddel(new Laegemiddel("Paracetamol", 1, 1.5, 2, "Ml"));
		storage.addLaegemiddel(new Laegemiddel("Fucidin", 0.025, 0.025, 0.025,
				"Styk"));
		storage.addLaegemiddel(new Laegemiddel("Methotrexat", 0.01, 0.015,
				0.02, "Styk"));

		opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12),
				storage.getAllPatienter().get(0), storage.getAllLaegemidler()
				.get(1), 123);

		opretPNOrdination(LocalDate.of(2015, 2, 12), LocalDate.of(2015, 2, 14),
				storage.getAllPatienter().get(0), storage.getAllLaegemidler()
				.get(0), 3);

		opretPNOrdination(LocalDate.of(2015, 1, 20), LocalDate.of(2015, 1, 25),
				storage.getAllPatienter().get(3), storage.getAllLaegemidler()
				.get(2), 5);

		opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12),
				storage.getAllPatienter().get(0), storage.getAllLaegemidler()
				.get(1), 123);

		opretDagligFastOrdination(LocalDate.of(2015, 1, 10),
				LocalDate.of(2015, 1, 12), storage.getAllPatienter().get(1),
				storage.getAllLaegemidler().get(1), 2, 0, 1, 0);

		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40),
				LocalTime.of(16, 0), LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };

		opretDagligSkaevOrdination(LocalDate.of(2015, 1, 23),
				LocalDate.of(2015, 1, 24), storage.getAllPatienter().get(1),
				storage.getAllLaegemidler().get(2), kl, an);
	}

}
