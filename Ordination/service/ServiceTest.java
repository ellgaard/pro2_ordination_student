package service;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import ordination.Laegemiddel;
import ordination.Patient;

import org.junit.Before;
import org.junit.Test;

public class ServiceTest {

	private Service service;
	private Patient patient;
	private Laegemiddel laegemiddel;

	@Before
	public void setUp() throws Exception {
		service = Service.getTestService();
		patient = service.createPatient("2505891859", "Anders Nielsen", 75);
		laegemiddel = new Laegemiddel("DSG", 0.05, 0.040, 0.035, "mg");
	}

	@Test
	public void testAnbefaletDosisPrDoegn1() {
		patient.setVaegt(24);
		double resultat = service.anbefaletDosisPrDoegn(patient, laegemiddel);
		assertEquals(1.2, resultat, 0.01);
	}

	@Test
	public void testAnbefaletDosisPrDoegn2() {
		patient.setVaegt(100);
		double resultat = service.anbefaletDosisPrDoegn(patient, laegemiddel);
		assertEquals(4, resultat, 0.01);
	}

	@Test
	public void testAnbefaletDosisPrDoegn3() {
		patient.setVaegt(130);
		double resultat = service.anbefaletDosisPrDoegn(patient, laegemiddel);
		assertEquals(4.55, resultat, 0.01);
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel1() {
		service.opretPNOrdination(LocalDate.of(2016, 02, 24),
				LocalDate.of(2016, 02, 26), patient, laegemiddel, 2);
		int resultat = service.antalOrdinationerPrVægtPrLægemiddel(0, 100,
				laegemiddel);
		assertEquals(1, resultat, 0);
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel2() {
		service.opretPNOrdination(LocalDate.of(2016, 02, 24),
				LocalDate.of(2016, 02, 26), patient, laegemiddel, 2);
		int resultat = service.antalOrdinationerPrVægtPrLægemiddel(0, 60,
				laegemiddel);
		assertEquals(0, resultat, 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAntalOrdinationerPrVægtPrLægemiddel3() {
		service.antalOrdinationerPrVægtPrLægemiddel(-1, 100, laegemiddel);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAntalOrdinationerPrVægtPrLægemiddel4() {
		service.antalOrdinationerPrVægtPrLægemiddel(100, 60, laegemiddel);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretPnOrdination1() {
		service.opretPNOrdination(LocalDate.of(2016, 03, 26),
				LocalDate.of(2016, 03, 24), patient, laegemiddel, 5);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretPnOrdination2() {
		service.opretPNOrdination(LocalDate.of(2016, 03, 24),
				LocalDate.of(2016, 03, 26), patient, laegemiddel, 0);
	}

}
